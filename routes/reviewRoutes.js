const express = require('express');
const reviewController = require('../controllers/reviewController');
const authController = require('../controllers/authController')

const router = express.Router({mergeParams: true});
router.use(authController.protect);
router.route('/user').post(reviewController.userAddReview);
router.route('/myreview').get(reviewController.getMyReview);
router
    .route('/')
    .get( authController.restrictTo('admin'),reviewController.getAllReviews)
    .post(
        reviewController.setTourUserId,
        reviewController.createReview
    );

router
    .route('/:id')
    .get(reviewController.getReview)
    .patch(reviewController.updateReview)
    .delete(reviewController.deleteReview);

module.exports = router;