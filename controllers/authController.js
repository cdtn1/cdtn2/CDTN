const crypto = require('crypto');
const { promisify } = require('util');
const jwt = require('jsonwebtoken');
const User = require('./../models/userModel');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');
const sendMail = require('./../utils/email');
require("dotenv").config();

let refreshTokens = [];

const generateActivationToken = (payload) => {
  return jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN_ACTIVATION_KEY,
  });
};

const generateAccessToken = (user) => {
  return jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN_ACCES_KEY
  });
};

const generateRefreshToken = (user) => {
  return jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN_REFRESH_KEY,
  });
};

const createSendToken = (user, statusCode, req, res, msg, refreshTokens, redirectToLogin) => {
  const accessToken = generateAccessToken(user);
  const refreshToken = generateRefreshToken(user);
  refreshTokens.push(refreshToken);
  res.cookie('jwt', refreshToken, {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
    secure: req.secure || req.headers['x-forwarded-proto'] === 'https'
  });

  // Remove password from output
  user.password = undefined;
  const {password, ...others} = user._doc;
  if(redirectToLogin)
    res.redirect(`${process.env.CLIENT_URL}/login`);
  else{
    res.status(statusCode).json({
      ...others,
      status: 'success',
      message: msg,
      accessToken
    });
  }
};

exports.signup = catchAsync(async (req, res, next) => {
  const { email } = req.body;
  const newUser = {
    email: req.body.email,
    name: req.body.name,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm,
  }; 
  const check = await User.findOne({ emai: newUser.email });
  if (check) {
    res.redirect(`${process.env.CLIENT_URL}/login`).status(400).json({ 
      status:"error",
      message:"This email already exists! Please create another account"
    })
    return next(new AppError('This email already exists', 400));
  }
  const activation_token = generateActivationToken(newUser);
  const url = `${process.env.SERVER_URL}/api/v1/users/finalregister/${activation_token}`
  const html = `Xin vui lòng kích vào đường link dưới để hoàn tất việc đăng kí! Thông báo này sẽ có hiệu lực 15p kể từ bây giờ.`
  sendMail(req.body.email, url, html);
  return res.status(200).json({
    status: 'success',
    message: 'Check your email and active account',
    data: {
      activeToken: activation_token,
    }
  })
});
  
exports.finalRegister = catchAsync(async (req, res, next) => {
  const { activation_token } = req.params
  const user = jwt.verify(activation_token, process.env.JWT_SECRET);
  const { name, email, password, passwordConfirm } = user;
  const newUser = new User({
    name,
    email,
    password,
    passwordConfirm
  });
  await newUser.save();
  createSendToken(newUser, 201, req, res, (msg = 'Enjoy ❤️'), refreshTokens, true);
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return next(new AppError('Please provide email and password!', 400));
    res.status(400).json({
      status: "error",
      message: "Missing Input"
    })
  }
  const user = await User.findOne({ email }).select('+password +accessToken');
  if (!user || !(await user.correctPassword(password, user.password))) {
    res.status(401).json({
      status: "error",
      message: "Incorrect email or password"
    })
    return next(new AppError('Incorrect email or password', 401));
  }
  createSendToken(user, 200, req, res, (msg = "Login Successfully"), refreshTokens, false);
});

exports.logout = (req, res) => {
  res.clearCookie('jwt')
  refreshTokens = refreshTokens.filter(
    (token) => token !== req.cookies.jwt
  );

  res.status(200).json({ status: 'success', message: 'You are loggedout' });
};

exports.requestRefreshToken = catchAsync(async (req, res, next) => {
  const refreshToken = req.cookies.jwt;
  if(!refreshToken) {
    return next(
      new AppError('Youre not authenticated, Please login again!', 401)
    );
  }
  if (!refreshTokens.includes(refreshToken)) {
    return next(
      new AppError('Refresh token is not valid,Please login again!', 403)
    );
  }
  const rs = await jwt.verify(refreshToken, process.env.JWT_SECRET);
  const user = await User.findOne({ id: rs._id});
  const newAccessToken = generateAccessToken(user);
  const newRefreshToken = generateRefreshToken(user);
  refreshTokens.push(newRefreshToken);
  res.cookie('jwt', newRefreshToken, {
    maxAge: 30 * 24 * 60 * 60 * 1000,
    httpOnly: true,
    secure: req.secure || req.headers['x-forwarded-proto'] == 'https',
    sameSite: 'none',
  });
  res.status(200).json({ accessToken: newAccessToken });
})

exports.protect = catchAsync(async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  } else if (req.cookies.jwt) {
    token = req.cookies.jwt;
  }
  if (!token) {
    return next(
      new AppError('You are not logged in! Please log in to get access.', 401)
    );
  }
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
  const currentUser = await User.findById(decoded.id);

  if (!currentUser) {
    return next(
      new AppError(
        'The user belonging to this token does no longer exist.',
        401
      )
    );
  }
  if (currentUser.changedPasswordAfter(decoded.iat)) {
    return next(
      new AppError('User recently changed password! Please log in again.', 401)
    );
  }
  req.user = currentUser;
  res.locals.user = currentUser;
  next();
});

exports.isLoggedIn = async (req, res, next) => {
  if (req.cookies.jwt) {
    try {
      const decoded = await promisify(jwt.verify)(
        req.cookies.jwt,
        process.env.JWT_SECRET
      );
      const currentUser = await User.findById(decoded.id);
      if (!currentUser) {
        return next();
      }
      if (currentUser.changedPasswordAfter(decoded.iat)) {
        return next();
      }
      res.locals.user = currentUser;
      return next();
    } catch (err) {
      return next();
    }
  }
  next();
};

exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(
        new AppError('You do not have permission to perform this action', 403)
      );
    }

    next();
  };
};

exports.forgotPassword = catchAsync(async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email }).select('+password');
  if (!user) {
    return next(new AppError('There is no user with email address.', 404));
  }
  const resetToken = user.createPasswordResetToken();
  await user.save({ validateBeforeSave: false });
  try {
    const url = `${process.env.SERVER_URL}/api/v1/users/resetPassword/${resetToken}`;
    const html = `Please click on the link below to change your password. This link will expire 15 minutes from now.`
    sendMail( req.body.email, url, html);
    res.status(200).json({
      status: 'success',
      message: 'Check your email and reset password',
      data: {
        token: resetToken
      }
    });
  } catch (err) {
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;
    await user.save({ validateBeforeSave: false });

    return next(
      new AppError('There was an error sending the email. Try again later!'),
      500
    );
  }
});

exports.resetPassword = catchAsync(async (req, res, next) => {
  const hashedToken = crypto
    .createHash('sha256')
    .update(req.params.resetToken)
    .digest('hex');

  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpires: { $gt: Date.now() }
  });
  if (!user) {
    return next(new AppError('Token is invalid or has expired', 400));
  }
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save();
  createSendToken(user, 200, req, res,(msg = "Reset Passwords Successfully") ,refreshTokens, false);
});

exports.updatePassword = catchAsync(async (req, res, next) => {
  // 1) Get user from collection
  const user = await User.findById(req.user.id).select('+password');

  // 2) Check if POSTed current password is correct
  if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
    return next(new AppError('Your current password is wrong.', 401));
  }

  // 3) If so, update password
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  await user.save();
  // User.findByIdAndUpdate will NOT work as intended!

  // 4) Log user in, send JWT
  createSendToken(user, 200, req, res, (msg = "Update Passwords Successfully"), refreshTokens, false);
});