const Review = require('./../models/reviewModel');
const factory = require('./handlerFactory')
const AppError = require('./../utils/appError');
const catchAsync = require('./../utils/catchAsync');

exports.setTourUserId = (req, res, next) => {
    if(!req.body.tour) req.body.tour = req.params.tourId;
    if(!req.body.user) req.body.user = req.user.id;
    next();
};

exports.getMyReview = catchAsync(async (req, res, next) => {
    const reviews = await Review.find({ user: req.user.id }).populate({
        path: 'tour',
    });
    res.status(200).json({
        status: 'success',
        reviews,
    });
}),

exports.userAddReview = catchAsync(async (req, res) => {
    const data = { ...req.body };
    data.user = req.user.id;
    const newReview = new Review(data);
    const review = await newReview.save();
    res.status(200).json({
        status: 'success',
        message: 'Created a review',
        review,
    });
}),

exports.getAllReviews = factory.getAll(Review);
exports.getReview = factory.getOne(Review);
exports.createReview = factory.createOne(Review);
exports.updateReview = factory.updateOne(Review);
exports.deleteReview = factory.deleteOne(Review);